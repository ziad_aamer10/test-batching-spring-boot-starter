package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Address;
import com.example.demo.model.Person;
import com.example.demo.repo.AddressRepository;
import com.example.demo.repo.PersonRepository;

@RestController
public class PersonController {

	@Autowired
	PersonRepository personRepo;
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	AddressRepository addressRepo;
	
//	@Transactional
	@PostMapping("/persons")
	public List<Person> savePersons(@RequestBody List<Person> persons){
	
		personRepo.saveAll(persons);
	
//		EntityTransaction tx= 	em.create
//		Transaction tx = session.beginTransaction();
//		   
//		for ( int i=0; i<100000; i++ ) {
//		    Customer customer = new Customer(.....);
//		    session.save(customer);
//		    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
//		        //flush a batch of inserts and release memory:
//		        session.flush();
//		        session.clear();
//		    }
//		}
//		   
//		tx.commit();
//		session.close();
//		persons.get(0).setFirstName("HUSSSSIIIIIIIIIIIIIIIIIIII");
		
		return persons;
	}
	
	@PostMapping("/addresses")
	public List<Address> saveAddresss(@RequestBody List<Address> add){
		
		List<Address> addresses = new ArrayList<>();
		addresses.add(new Address(1, "City", "Country"));
		addresses.add(new Address(2, "City", "Country"));
		addresses.add(new Address(3, "City", "Country"));
		addresses.add(new Address(4, "City", "Country"));
		addresses.add(new Address(5, "City", "Country"));
		addresses.add(new Address(6, "City", "Country"));
	
		addressRepo.saveAll(addresses);
	
//		EntityTransaction tx= 	em.create
//		Transaction tx = session.beginTransaction();
//		   
//		for ( int i=0; i<100000; i++ ) {
//		    Customer customer = new Customer(.....);
//		    session.save(customer);
//		    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
//		        //flush a batch of inserts and release memory:
//		        session.flush();
//		        session.clear();
//		    }
//		}
//		   
//		tx.commit();
//		session.close();
		
		return addresses;
	}
}
