package com.example.demo.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.goaml.core.entity.sar.Transaction;
import com.example.demo.repo.TransactionRepository;

@RestController
public class TranasactionController {

	@Autowired
	TransactionRepository transactionRepo;
	@PersistenceContext
	EntityManager em;
	
	@PostMapping("/transactions")
	public List<Transaction> savePersons(@RequestBody List<Transaction> transs){
	
		transactionRepo.saveAll(transs);
	
		return transs;
	}
}
