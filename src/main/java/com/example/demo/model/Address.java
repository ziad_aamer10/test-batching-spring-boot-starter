
package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "address", schema = "seq")
public class Address {

	public Address(long id, String city, String country) {
		super();
		this.id = id;
		this.city = city;
		this.country = country;
	}
	
	

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}



	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_address")
    @GenericGenerator(
            name = "hilo_sequence_generator_address",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
                    parameters = {
                            @Parameter(name = "sequence_name", value = "hilo_sequence_generator_address"),
                            @Parameter(name = "initial_value", value = "1"),
                            @Parameter(name = "increment_size", value = "10"),
                            @Parameter(name = "optimizer", value = "pooled")
                    })
	private long id;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id")
	private Person person;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@JsonIgnore
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
