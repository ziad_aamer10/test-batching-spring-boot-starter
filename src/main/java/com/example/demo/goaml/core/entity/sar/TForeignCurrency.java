package com.example.demo.goaml.core.entity.sar;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class TForeignCurrency {
	
	
	protected String foreignCurrencyCode;
	
	private BigDecimal foreignAmount;
	
	private BigDecimal foreignExchangeRate;

	
	public TForeignCurrency() {
		
	}

	public String getForeignCurrencyCode() {
		return foreignCurrencyCode;
	}

	public void setForeignCurrencyCode(String foreignCurrencyCode) {
		this.foreignCurrencyCode = foreignCurrencyCode;
	}

	public BigDecimal getForeignAmount() {
		return foreignAmount;
	}

	public void setForeignAmount(BigDecimal foreignAmount) {
		this.foreignAmount = foreignAmount;
	}

	public BigDecimal getForeignExchangeRate() {
		return foreignExchangeRate;
	}

	public void setForeignExchangeRate(BigDecimal foreignExchangeRate) {
		this.foreignExchangeRate = foreignExchangeRate;
	}
	
	

}
