package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(schema = "target")
@Audited
public class TTransItem {

	
		@Id
//		@GeneratedValue(strategy = GenerationType.IDENTITY)
		  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TTransItem")
		  @GenericGenerator(
		          name = "hilo_sequence_generator_TTransItem",
		          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
		          parameters = {
		                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TTransItem"),
		                  @Parameter(name = "initial_value", value = "1"),
		                  @Parameter(name = "increment_size", value = "10"),
		                  @Parameter(name = "optimizer", value = "hilo")
		          })
		private int id;
		
		
//		@ManyToOne(fetch = FetchType.LAZY)
//	    @JoinColumn(name = "Activity_id")
//	    private Activity activity;
		
		@ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "transaction_id")
	    private Transaction transaction;
		
		
		private String itemType;
		
		private String itemMake;
		private String description;
		
		private String previouslyRegisteredTo;
		
		private String presentlyRegisteredTo;
		
		private String estimatedValue;
		
		private String statusCode;
		private String statusComments;
		private String disposedValue;
		private String currencyCode;
		
		@Column(name="[SIZE]")
//		@Column(name="SIZE")
		private String size;
		
		private String sizeUom;
		
		@Embedded
		private TAddressTransItem address;
		
		private String registrationDate;
		private String registrationNumber;
		private String identificationNumber;
		private String comments;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
//		@JsonIgnore
//		public Activity getActivity() {
//			return activity;
//		}
//		public void setActivity(Activity activity) {
//			this.activity = activity;
//		}
		
		
		@JsonIgnore
		public Transaction getTransaction() {
			return transaction;
		}
		public void setTransaction(Transaction transaction) {
			this.transaction = transaction;
		}
		public String getItemType() {
			return itemType;
		}
		public void setItemType(String itemType) {
			this.itemType = itemType;
		}
		public String getItemMake() {
			return itemMake;
		}
		public void setItemMake(String itemMake) {
			this.itemMake = itemMake;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getPreviouslyRegisteredTo() {
			return previouslyRegisteredTo;
		}
		public void setPreviouslyRegisteredTo(String previouslyRegisteredTo) {
			this.previouslyRegisteredTo = previouslyRegisteredTo;
		}
		public String getPresentlyRegisteredTo() {
			return presentlyRegisteredTo;
		}
		public void setPresentlyRegisteredTo(String presentlyRegisteredTo) {
			this.presentlyRegisteredTo = presentlyRegisteredTo;
		}
		public String getEstimatedValue() {
			return estimatedValue;
		}
		public void setEstimatedValue(String estimatedValue) {
			this.estimatedValue = estimatedValue;
		}
		public String getStatusCode() {
			return statusCode;
		}
		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}
		public String getStatusComments() {
			return statusComments;
		}
		public void setStatusComments(String statusComments) {
			this.statusComments = statusComments;
		}
		public String getDisposedValue() {
			return disposedValue;
		}
		public void setDisposedValue(String disposedValue) {
			this.disposedValue = disposedValue;
		}
		public String getCurrencyCode() {
			return currencyCode;
		}
		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}
		public String getSize() {
			return size;
		}
		public void setSize(String size) {
			this.size = size;
		}
		public String getSizeUom() {
			return sizeUom;
		}
		public void setSizeUom(String sizeUom) {
			this.sizeUom = sizeUom;
		}
		public TAddressTransItem getAddress() {
			return address;
		}
		public void setAddress(TAddressTransItem address) {
			this.address = address;
		}
		public String getRegistrationDate() {
			return registrationDate;
		}
		public void setRegistrationDate(String registrationDate) {
			this.registrationDate = registrationDate;
		}
		public String getRegistrationNumber() {
			return registrationNumber;
		}
		public void setRegistrationNumber(String registrationNumber) {
			this.registrationNumber = registrationNumber;
		}
		public String getIdentificationNumber() {
			return identificationNumber;
		}
		public void setIdentificationNumber(String identificationNumber) {
			this.identificationNumber = identificationNumber;
		}
		public String getComments() {
			return comments;
		}
		public void setComments(String comments) {
			this.comments = comments;
		}
		
		
		
}
