package com.example.demo.goaml.core.entity.sar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


@Embeddable
public class EntityPhones {
	
	@OneToMany(mappedBy = "entity",
			cascade = CascadeType.ALL,
	 orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TPhone> phone;
	
	public EntityPhones(){
		phone = new ArrayList<TPhone>();
	}
	
	public List<TPhone> getPhone() {
		return phone;
	}

	public void setPhone(List<TPhone> phone) {
		
		if(phone == null)
		{
			this.phone.clear();
			return;
		}
		this.phone = phone;
	}
	
}
