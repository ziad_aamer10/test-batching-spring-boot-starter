package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema="target")
@Audited
public class EmployerPhoneId {
	
	
	@Id
 //   @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_EmployerPhoneId")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_EmployerPhoneId",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_EmployerPhoneId"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int Id;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private TPerson person;
	private String tphContactType;
	
	private String tphCommunicationType;
	
	private String tphCountryPrefix;
	
	private String tphNumber;
	
	private String tphExtension;
	private String phoneComments;
	
	
	
	
	public EmployerPhoneId() {
		super();
	}
	
	public EmployerPhoneId(int id, String tphContactType, String tphCommunicationType, String tphCountryPrefix,
			String tphNumber, String tphExtension, String phoneComments) {
		super();
		Id = id;
		
		this.tphContactType = tphContactType;
		this.tphCommunicationType = tphCommunicationType;
		this.tphCountryPrefix = tphCountryPrefix;
		this.tphNumber = tphNumber;
		this.tphExtension = tphExtension;
		this.phoneComments = phoneComments;
	}

	

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTphContactType() {
		return tphContactType;
	}
	public void setTphContactType(String tphContactType) {
		this.tphContactType = tphContactType;
	}
	public String getTphCommunicationType() {
		return tphCommunicationType;
	}
	public void setTphCommunicationType(String tphCommunicationType) {
		this.tphCommunicationType = tphCommunicationType;
	}
	public String getTphCountryPrefix() {
		return tphCountryPrefix;
	}
	public void setTphCountryPrefix(String tphCountryPrefix) {
		this.tphCountryPrefix = tphCountryPrefix;
	}
	public String getTphNumber() {
		return tphNumber;
	}
	public void setTphNumber(String tphNumber) {
		this.tphNumber = tphNumber;
	}
	public String getTphExtension() {
		return tphExtension;
	}
	public void setTphExtension(String tphExtension) {
		this.tphExtension = tphExtension;
	}
	public String getPhoneComments() {
		return phoneComments;
	}
	public void setPhoneComments(String phoneComments) {
		this.phoneComments = phoneComments;
	}
	
	@JsonIgnore
	public TPerson getPerson() {
		return person;
	}

	public void setPerson(TPerson person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "TPhone [Id=" + Id + ", tphContactType=" + tphContactType + ", tphCommunicationType="
				+ tphCommunicationType + ", tphCountryPrefix=" + tphCountryPrefix + ", tphNumber=" + tphNumber
				+ ", tphExtension=" + tphExtension + ", phoneComments=" + phoneComments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + ((phoneComments == null) ? 0 : phoneComments.hashCode());
		result = prime * result + ((tphCommunicationType == null) ? 0 : tphCommunicationType.hashCode());
		result = prime * result + ((tphContactType == null) ? 0 : tphContactType.hashCode());
		result = prime * result + ((tphCountryPrefix == null) ? 0 : tphCountryPrefix.hashCode());
		result = prime * result + ((tphExtension == null) ? 0 : tphExtension.hashCode());
		result = prime * result + ((tphNumber == null) ? 0 : tphNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployerPhoneId other = (EmployerPhoneId) obj;
		if (Id != other.Id)
			return false;
		if (phoneComments == null) {
			if (other.phoneComments != null)
				return false;
		} else if (!phoneComments.equals(other.phoneComments))
			return false;
		if (tphCommunicationType == null) {
			if (other.tphCommunicationType != null)
				return false;
		} else if (!tphCommunicationType.equals(other.tphCommunicationType))
			return false;
		if (tphContactType == null) {
			if (other.tphContactType != null)
				return false;
		} else if (!tphContactType.equals(other.tphContactType))
			return false;
		if (tphCountryPrefix == null) {
			if (other.tphCountryPrefix != null)
				return false;
		} else if (!tphCountryPrefix.equals(other.tphCountryPrefix))
			return false;
		if (tphExtension == null) {
			if (other.tphExtension != null)
				return false;
		} else if (!tphExtension.equals(other.tphExtension))
			return false;
		if (tphNumber == null) {
			if (other.tphNumber != null)
				return false;
		} else if (!tphNumber.equals(other.tphNumber))
			return false;
		return true;
	}
	
	
	

}
