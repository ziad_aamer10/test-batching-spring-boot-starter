package com.example.demo.goaml.core.entity.sar;



import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Embeddable
public class EntityAddresses {
	
	
	@OneToMany(mappedBy = "entity",
			cascade = CascadeType.ALL,
	 orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TAddress> address;
	
	EntityAddresses(){
		address = new ArrayList<TAddress>();
	}

	public List<TAddress> getAddress() {
		return address;
	}

	public void setAddress(List<TAddress> address) {
		
		if(address == null) {
			this.address.clear();
			return;
		}
		this.address = address;
		
//		for(TAddress tAddress: address)
//			tAddress.setEntity(entity);
		
	}
	

	
	
	
	

}
