package com.example.demo.goaml.core.entity.sar;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class TTo {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TTo")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TTo",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TTo"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;

	private String toFundsCode;
	
	private String toFundsComment;
	
	@Embedded
	private TForeignCurrency toForeignCurrency;
	
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	 @JoinColumn(name = "t_account_id")
	private TAccount toAccount;
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	 @JoinColumn(name = "t_person_id")
	private TPerson toPerson;
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	 @JoinColumn(name = "t_entity_id")
	private TEntity toEntity;
	
	private String toCountry;
	
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id")
	private Transaction transaction;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToFundsCode() {
		return toFundsCode;
	}

	public void setToFundsCode(String toFundsCode) {
		this.toFundsCode = toFundsCode;
	}

	public String getToFundsComment() {
		return toFundsComment;
	}

	public void setToFundsComment(String toFundsComment) {
		this.toFundsComment = toFundsComment;
	}

	public TForeignCurrency getToForeignCurrency() {
		return toForeignCurrency;
	}

	public void setToForeignCurrency(TForeignCurrency toForeignCurrency) {
		this.toForeignCurrency = toForeignCurrency;
	}

	public TAccount getToAccount() {
		return toAccount;
	}

	public void setToAccount(TAccount toAccount) {
		this.toAccount = toAccount;
		if(toAccount!=null)
			this.toAccount.settTo(this);
	}

	public TPerson getToPerson() {
		return toPerson;
	}

	public void setToPerson(TPerson toPerson) {
		this.toPerson = toPerson;
		if(toPerson!=null)
			this.toPerson.settTo(this);
	}

	public TEntity getToEntity() {
		return toEntity;
	}

	public void setToEntity(TEntity toEntity) {
		this.toEntity = toEntity;
		
		if(toEntity!=null)
			this.toEntity.settTo(this);
	}

	public String getToCountry() {
		return toCountry;
	}

	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}
	
	@JsonIgnore
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	

}
