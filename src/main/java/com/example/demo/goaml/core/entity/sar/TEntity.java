package com.example.demo.goaml.core.entity.sar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Ziad
 *
 */
@Entity
@Table(schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class TEntity {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TEntity")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TEntity",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TEntity"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;
	
	private String name;
	
	private String commercialName;
	
	private String incorporationLegalForm;
	
	private String incorporationNumber;
	private String business;
	
//	@OneToMany(mappedBy = "entity",
//			cascade = CascadeType.ALL,
//			orphanRemoval = true)
//	private List<TPhone> phones = new ArrayList<TPhone>();
	
	@Embedded
	private EntityPhones phones ;
	
	@Embedded
	private EntityAddresses addresses;
	
//	@OneToMany(mappedBy = "entity",
//			cascade = CascadeType.ALL,
//	 orphanRemoval = true, fetch = FetchType.LAZY)
//	private List<TAddress> addresses = new ArrayList<TAddress>();
	
	private String email;
	
	private String url;
	
	private String incorporationState;
	
	private String incorporationCountryCode;
	
	@OneToMany(mappedBy = "entity",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval=true)
	private List<DirectorId> directorId;
	
	private String incorporationDate;
	
	private Boolean businessClosed;
	
	private String dateBusinessClosed;
	
	private String taxNumber;
	
	private String taxRegNumber;
	private String comments;
	
//	@OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "reportPartyType_id")
//    private ReportPartyType reportPartyType;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
	private TAccount account;
	
	
	@OneToOne(mappedBy = "fromEntity",
			fetch = FetchType.LAZY)
	private TFrom tFrom;
	
	@OneToOne(mappedBy = "toEntity",
			fetch = FetchType.LAZY)
	private TTo tTo;
	
	private String entityNumber;
	
	@Column(name="is_reviewed")
	private Boolean isReviewed;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Transient
	private String changeBeginDate;
	
	public TEntity(){
		
		this.directorId= new ArrayList<DirectorId>();
		this.phones = new EntityPhones();
		this.addresses = new EntityAddresses();
	
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommercialName() {
		return commercialName;
	}
	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}
	public String getIncorporationLegalForm() {
		return incorporationLegalForm;
	}
	public void setIncorporationLegalForm(String incorporationLegalForm) {
		this.incorporationLegalForm = incorporationLegalForm;
	}
	public String getIncorporationNumber() {
		return incorporationNumber;
	}
	public void setIncorporationNumber(String incorporationNumber) {
		this.incorporationNumber = incorporationNumber;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
//	public List<TPhone> getPhones() {
//		return phones;
//	}
//	public void setPhones(List<TPhone> phones) {
//		this.phones = phones;
//		
//		for(TPhone phone: this.phones) {
//			phone.setEntity(this);
//		}
//	}
//	public List<TAddress> getAddresses() {
//		return addresses;
//	}
//	public void setAddresses(List<TAddress> addresses) {
//		this.addresses = addresses;
//		
//		for(TAddress address: this.addresses) {
//			address.setEntity(this);
//		}
//	}
	
	public EntityPhones getPhones() {
		return phones;
	}
	
//	public void setPhones(EntityPhones phones) {
//			this.phones = phones;
//			
//			if(phones!=null && phones.getPhone()!=null)
//				for(TPhone phone: phones.getPhone())
//					phone.setEntity(this);	
//	}
	public void setPhones(EntityPhones phones) {
		
		if(phones==null)
		{
			this.phones = new EntityPhones();
			return;
		}
		
		this.phones = phones;
		
		if(phones.getPhone()!=null)
			for(TPhone phone: phones.getPhone())
				phone.setEntity(this);	
		
	}
	
	
	public EntityAddresses getAddresses() {
		return addresses;
	}
	
	public void setAddresses(EntityAddresses addresses) {
		
		if(addresses==null)
		{
			this.addresses = new EntityAddresses();
			return;
		}
		
		this.addresses = addresses;
		if(addresses.getAddress()!=null)
			for(TAddress address2: addresses.getAddress()) {
				address2.setEntity(this);
			}
	}
	
	public void addOneDirectorId(DirectorId oneDirectorId) {
		//prevent endless loop
		if (oneDirectorId == null || directorId.contains(oneDirectorId))
			return;
		//add new account
		directorId.add(oneDirectorId);
		//set myself into the twitter account
		oneDirectorId.setEntity(this);
	}

	public void removeOneDirectorId(DirectorId oneDirectorId) {
		//prevent endless loop
		if (oneDirectorId == null || !directorId.contains(oneDirectorId))
			return;
		//remove the account
		directorId.remove(oneDirectorId);
		//remove myself from the twitter account
		oneDirectorId.setEntity(null);
	}
  
	public List<DirectorId> getDirectorId() {
		 //defensive copy, nobody will be able to change 
	    //the list from the outside
		return directorId;
	}
	
	public void setDirectorId(List<DirectorId> directorId) {
		
		this.directorId.clear();

		if(directorId!=null) {
			for(DirectorId dir: directorId) {
				dir.setEntity(this);
				this.directorId.add(dir);
			}
		}
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIncorporationState() {
		return incorporationState;
	}
	public void setIncorporationState(String incorporationState) {
		this.incorporationState = incorporationState;
	}
	public String getIncorporationCountryCode() {
		return incorporationCountryCode;
	}
	public void setIncorporationCountryCode(String incorporationCountryCode) {
		this.incorporationCountryCode = incorporationCountryCode;
	}
	public String getIncorporationDate() {
		return incorporationDate;
	}
	public void setIncorporationDate(String incorporationDate) {
		this.incorporationDate = incorporationDate;
	}
	public Boolean getBusinessClosed() {
		return businessClosed;
	}
	public void setBusinessClosed(Boolean businessClosed) {
		this.businessClosed = businessClosed;
	}
	public String getDateBusinessClosed() {
		return dateBusinessClosed;
	}
	public void setDateBusinessClosed(String dateBusinessClosed) {
		this.dateBusinessClosed = dateBusinessClosed;
	}
	public String getTaxNumber() {
		return taxNumber;
	}
	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}
	public String getTaxRegNumber() {
		return taxRegNumber;
	}
	public void setTaxRegNumber(String taxRegNumber) {
		this.taxRegNumber = taxRegNumber;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
//	@JsonIgnore
//	public ReportPartyType getReportPartyType() {
//		return reportPartyType;
//	}
//	public void setReportPartyType(ReportPartyType reportPartyType) {
//		this.reportPartyType = reportPartyType;
//	}
	
	@JsonIgnore
	public TAccount getAccount() {
		return account;
	}
	public void setAccount(TAccount account) {
		this.account = account;
	}
	
	@JsonIgnore
	public TFrom gettFrom() {
		return tFrom;
	}
	public void settFrom(TFrom tFrom) {
		this.tFrom = tFrom;
	}
	
	@JsonIgnore
	public TTo gettTo() {
		return tTo;
	}
	public void settTo(TTo tTo) {
		this.tTo = tTo;
	}
	public String getEntityNumber() {
		return entityNumber;
	}
	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}
	public String getChangeBeginDate() {
		return changeBeginDate;
	}

	public void setChangeBeginDate(String changeBeginDate) {
		this.changeBeginDate = changeBeginDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "TEntity [id=" + id + ", name=" + name + ", commercialName=" + commercialName
				+ ", incorporationLegalForm=" + incorporationLegalForm + ", incorporationNumber=" + incorporationNumber
				+ ", business=" + business + ", phones=" + phones + ", addresses=" + addresses + ", email=" + email
				+ ", url=" + url + ", incorporationState=" + incorporationState + ", incorporationCountryCode="
				+ incorporationCountryCode + ", directorId=" + directorId + ", incorporationDate=" + incorporationDate
				+ ", businessClosed=" + businessClosed + ", dateBusinessClosed=" + dateBusinessClosed + ", taxNumber="
				+ taxNumber + ", taxRegNumber=" + taxRegNumber + ", comments=" + comments +  ", account=" + account + ", tFrom=" + tFrom + ", tTo=" + tTo + ", entityNumber="
				+ entityNumber + "]";
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}
	
	
	public void setTEntityIdsZeros() {
		
		this.setId(0);

		if(this.addresses!=null && this.addresses.getAddress() !=null) {
			for(TAddress address2: this.addresses.getAddress()) {
				address2.setId(0);
			}
		}

		if(this.phones!=null && this.phones.getPhone()!=null) {
			for(TPhone phone: this.phones.getPhone()) {
				phone.setId(0);	
			}
		}
		if(this.directorId!=null) {
			for(DirectorId dir: this.directorId) {
				dir.setTPersonIdsZeros(dir);
			}
		}
		
	}
}
