package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(schema="target")
@Audited
public class TPersonIdentification {
	
	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TPersonIdentification")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TPersonIdentification",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TPersonIdentification"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })  
	private int Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private TPerson person;
	private String type;

	@Column(name="[NUMBER]")
//	@Column(name = "NUMBER")
	private String number;
	
	private String issueDate;
	
	private String expiryDate;
	
	private String issuedBy;
	
	private String issueCountry;
	private String comments;
	

	public TPersonIdentification() {
		super();
	}
	
	public TPersonIdentification(int id,  String type, String number, String issueDate,
			String expiryDate, String issuedBy, String issueCountry, String comments) {
		super();
		Id = id;
		
		this.type = type;
		this.number = number;
		this.issueDate = issueDate;
		this.expiryDate = expiryDate;
		this.issuedBy = issuedBy;
		this.issueCountry = issueCountry;
		this.comments = comments;
	}

	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getIssuedBy() {
		return issuedBy;
	}
	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}
	public String getIssueCountry() {
		return issueCountry;
	}
	public void setIssueCountry(String issueCountry) {
		this.issueCountry = issueCountry;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	@JsonIgnore
	public TPerson getPerson() {
		return person;
	}

	public void setPerson(TPerson person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "TPersonIdentification [Id=" + Id + ", type=" + type + ", number=" + number + ", issueDate=" + issueDate
				+ ", expiryDate=" + expiryDate + ", issuedBy=" + issuedBy + ", issueCountry=" + issueCountry
				+ ", identificationComments=" + comments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result + ((issueCountry == null) ? 0 : issueCountry.hashCode());
		result = prime * result + ((issueDate == null) ? 0 : issueDate.hashCode());
		result = prime * result + ((issuedBy == null) ? 0 : issuedBy.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TPersonIdentification other = (TPersonIdentification) obj;
		if (Id != other.Id)
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (issueCountry == null) {
			if (other.issueCountry != null)
				return false;
		} else if (!issueCountry.equals(other.issueCountry))
			return false;
		if (issueDate == null) {
			if (other.issueDate != null)
				return false;
		} else if (!issueDate.equals(other.issueDate))
			return false;
		if (issuedBy == null) {
			if (other.issuedBy != null)
				return false;
		} else if (!issuedBy.equals(other.issuedBy))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}


	
	

}
