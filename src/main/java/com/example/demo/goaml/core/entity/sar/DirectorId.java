package com.example.demo.goaml.core.entity.sar;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Audited
public class DirectorId extends TPerson implements Serializable{
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String role;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "entity_id")
    private TEntity entity;


	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	@JsonIgnore
	public TEntity getEntity() {
		return entity;
	}

	public void setEntity(TEntity entity) {
	
		this.entity = entity;
	}

//	private boolean sameAsFormer(TEntity newEntity) {
//		return entity==null? newEntity == null : entity.equals(newEntity);
//	}
	
	
	
	
	

}
