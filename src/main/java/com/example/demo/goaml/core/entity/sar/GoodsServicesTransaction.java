package com.example.demo.goaml.core.entity.sar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Embeddable
public class GoodsServicesTransaction {
	
	
	@OneToMany(mappedBy = "transaction",
			cascade = CascadeType.ALL,
			 orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TTransItem> item;
	
	
	public GoodsServicesTransaction() {
		item = new ArrayList<>();
	}

	public List<TTransItem> getItem() {
		return item;
	}

	public void setItem(List<TTransItem> item) {
		
		if(item==null) {
			this.item.clear();
			return;
		}
		
		this.item = item;
	}
	
	
	
	

}
