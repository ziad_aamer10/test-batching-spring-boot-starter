package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Embeddable;

@Embeddable
public class TAddressTransItem {
	
	private String addressType;
    private String address;
    private String town;
    private String city;
    private String zip;
    private String countryCode;
    private String state;
    private String addressComments;
    
    
    
	public TAddressTransItem() {
		super();
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddressComments() {
		return addressComments;
	}
	public void setAddressComments(String addressComments) {
		this.addressComments = addressComments;
	}
    
    
    

}
