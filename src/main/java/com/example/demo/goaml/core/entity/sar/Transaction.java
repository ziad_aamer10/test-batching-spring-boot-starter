package com.example.demo.goaml.core.entity.sar;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="[TRANSACTION]", schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class Transaction {
	
	@Id
	//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_Transaction")
	@GenericGenerator(
			name = "hilo_sequence_generator_Transaction",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "hilo_sequence_generator_Transaction"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "10"),
					@Parameter(name = "optimizer", value = "pooled")
			})
	private long id;

	private String transactionnumber;
	
	private String internalRefNumber;
	
	private String transactionLocation;
	
	private String transactionDescription;
	
	private String dateTransaction;
	
	private String teller;
	
	private String authorized;
	
	@Embedded
	@NotNull
	@Valid
	private LateDeposit lateDeposit;
	
	private String datePosting;
	
	private String valueDate;
	
	private String transmodeCode;
	
	private String transmodeComment;
	
	private BigDecimal amountLocal;
	
	//private Report.Transaction.InvolvedParties involvedParties;
	
	
	@OneToOne(mappedBy = "transactionMyClient",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.LAZY)
	private TFromMyClient tFromMyClient;
	
	@OneToOne(mappedBy = "transaction",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.LAZY)
	private TFrom tFrom;
	
	@OneToOne(mappedBy = "transactionMyClient",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.LAZY)
	private TToMyClient tToMyClient;
	
	@OneToOne(mappedBy = "transaction",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.LAZY)
	private TTo tTo;
	
	//private Report.Transaction.GoodsServices goodsServices;
	
	private String comments;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "report_id")
//	private Report report;
//	
	private Boolean isGoodsServices;
	@Embedded
	private GoodsServicesTransaction goodsServices;
	
	@Column(name="valid", columnDefinition = "bit default 0")
	private Boolean vaild = false;
	
	
	public Transaction(){
		this.lateDeposit = new LateDeposit();
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTransactionnumber() {
		return transactionnumber;
	}

	public void setTransactionnumber(String transactionnumber) {
		this.transactionnumber = transactionnumber;
	}

	public String getInternalRefNumber() {
		return internalRefNumber;
	}

	public void setInternalRefNumber(String internalRefNumber) {
		this.internalRefNumber = internalRefNumber;
	}

	public String getTransactionLocation() {
		return transactionLocation;
	}

	public void setTransactionLocation(String transactionLocation) {
		this.transactionLocation = transactionLocation;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public String getDateTransaction() {
		return dateTransaction;
	}

	public void setDateTransaction(String dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	public String getTeller() {
		return teller;
	}

	public void setTeller(String teller) {
		this.teller = teller;
	}

	public String getAuthorized() {
		return authorized;
	}

	public void setAuthorized(String authorized) {
		this.authorized = authorized;
	}

	public String getDatePosting() {
		return datePosting;
	}

	public void setDatePosting(String datePosting) {
		this.datePosting = datePosting;
	}

	public String getTransmodeCode() {
		return transmodeCode;
	}

	public void setTransmodeCode(String transmodeCode) {
		this.transmodeCode = transmodeCode;
	}

	public String getTransmodeComment() {
		return transmodeComment;
	}

	public void setTransmodeComment(String transmodeComment) {
		this.transmodeComment = transmodeComment;
	}

	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	public TFromMyClient gettFromMyClient() {
		return tFromMyClient;
	}

	public void settFromMyClient(TFromMyClient tFromMyClient) {
		this.tFromMyClient = tFromMyClient;
		
		if(tFromMyClient!=null)
			this.tFromMyClient.setTransaction(this);
	}

	public TFrom gettFrom() {
		return tFrom;
	}

	public void settFrom(TFrom tFrom) {
		this.tFrom = tFrom;
		
		if(tFrom!=null)
			this.tFrom.setTransaction(this);
	}

	public TToMyClient gettToMyClient() {
		return tToMyClient;
	}

	public void settToMyClient(TToMyClient tToMyClient) {
		this.tToMyClient = tToMyClient;
		if(tToMyClient!=null)
			this.tToMyClient.setTransaction(this);
	}

	public TTo gettTo() {
		return tTo;
	}

	public void settTo(TTo tTo) {
		this.tTo = tTo;
		if(tTo!=null)
			this.tTo.setTransaction(this);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public LateDeposit getLateDeposit() {
		return lateDeposit;
	}

	public void setLateDeposit(LateDeposit lateDeposit) {
		this.lateDeposit = lateDeposit;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	
//	@JsonIgnore
//	public Report getReport() {
//		return report;
//	}
//
//	public void setReport(Report report) {
//		this.report = report;
//	}

	public Boolean getIsGoodsServices() {
		return isGoodsServices;
	}

	public void setIsGoodsServices(Boolean isGoodsServices) {
		this.isGoodsServices = isGoodsServices;
	}

	public GoodsServicesTransaction getGoodsServices() {
		return goodsServices;
	}

	public void setGoodsServices(GoodsServicesTransaction goodsServices) {
		
		if(goodsServices==null)
		{
			this.goodsServices = new GoodsServicesTransaction();
			return;
		}
		
		this.goodsServices = goodsServices;
		if(goodsServices.getItem()!=null)
			for(TTransItem address2: goodsServices.getItem()) {
				address2.setTransaction(this);
			}
		
		
//		this.goodsServices = goodsServices;
//		
//		if(goodsServices!=null&&goodsServices.getItem()!=null)
//			for(TTransItem tTransItem: goodsServices.getItem())
//				tTransItem.setTransaction(this);
	}
	
	
	
	

}
