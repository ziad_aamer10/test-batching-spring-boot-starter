package com.example.demo.goaml.core.entity.sar;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class TFrom {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TFrom")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TFrom",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TFrom"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;
	
	
	private String fromFundsCode;

	private String fromFundsComment;
	
	@Embedded
	private TForeignCurrency fromForeignCurrency;
	
	@OneToOne(fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name = "t_conductor_id")
	private TPerson tConductor;
	
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinColumn(name = "t_account_id")
	private TAccount fromAccount;
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinColumn(name = "t_person_id")
	private TPerson fromPerson;
	
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinColumn(name = "t_entity_id")
	private TEntity fromEntity;
	
	private String fromCountry;
	
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id")
	private Transaction transaction;
	
	private Boolean isConductor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFromFundsCode() {
		return fromFundsCode;
	}

	public void setFromFundsCode(String fromFundsCode) {
		this.fromFundsCode = fromFundsCode;
	}

	public String getFromFundsComment() {
		return fromFundsComment;
	}

	public void setFromFundsComment(String fromFundsComment) {
		this.fromFundsComment = fromFundsComment;
	}

	public TPerson getFromPerson() {
		return fromPerson;
	}

	public void setFromPerson(TPerson fromPerson) {
		this.fromPerson = fromPerson;
		
		if(fromPerson!=null)
			this.fromPerson.settFrom(this);
	}
	
	
	

	public TPerson gettConductor() {
		return tConductor;
	}

	public void settConductor(TPerson tConductor) {
		this.tConductor = tConductor;
		
		if(tConductor!=null)
			this.tConductor.settFromConductor(this);
	}

	public String getFromCountry() {
		return fromCountry;
	}

	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}

	public TForeignCurrency getFromForeignCurrency() {
		return fromForeignCurrency;
	}

	public void setFromForeignCurrency(TForeignCurrency fromForeignCurrency) {
		this.fromForeignCurrency = fromForeignCurrency;
	}

	public TAccount getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(TAccount fromAccount) {
		this.fromAccount = fromAccount;
		
		if(fromAccount!=null)
			this.fromAccount.settFrom(this);
	}

	public TEntity getFromEntity() {
		return fromEntity;
	}

	public void setFromEntity(TEntity fromEntity) {
		this.fromEntity = fromEntity;
		
		if(fromEntity!=null)
			this.fromEntity.settFrom(this);
	}
	
	@JsonIgnore
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Boolean getIsConductor() {
		return isConductor;
	}

	public void setIsConductor(Boolean isConductor) {
		this.isConductor = isConductor;
	}

	
	

}
