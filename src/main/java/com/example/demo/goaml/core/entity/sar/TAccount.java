package com.example.demo.goaml.core.entity.sar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class TAccount {

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TAccount")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TAccount",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TAccount"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;

	private String institutionName;

	private String institutionCode;
	private String swift;

	private Boolean nonBankInstitution;
	private String branch;

	private String account;

	private String currencyCode;

	private String accountName;
	private String iban;

	private String clientNumber;


	private String personalAccountType;

	@OneToOne(mappedBy = "account",
			cascade = CascadeType.ALL, 
			fetch = FetchType.LAZY, orphanRemoval = true)
	private TEntity tEntity;

	@OneToMany(mappedBy = "account",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Signatory> signatory;

	private String opened;

	private String closed;
	private BigDecimal balance;

	private String dateBalance;

	private String statusCode;

	private String beneficiary;


	private String beneficiaryComment;
	private String comments;
	private Boolean isEntity;
	
	@Column(name="is_reviewed")
	private Boolean isReviewed;

//	@OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "reportPartyType_id")
//	private ReportPartyType reportPartyType;

	@OneToOne(mappedBy = "fromAccount",
			fetch = FetchType.LAZY)
	private TFrom tFrom;

	@OneToOne(mappedBy = "toAccount",
			fetch = FetchType.LAZY)
	private TTo tTo;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Transient
	private String changeBeginDate;
	
	public TAccount() {
		
		//this.tEntity = new TEntity();
		this.signatory = new ArrayList<>();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public String getSwift() {
		return swift;
	}
	public void setSwift(String swift) {
		this.swift = swift;
	}
	public Boolean getNonBankInstitution() {
		return nonBankInstitution;
	}
	public void setNonBankInstitution(Boolean nonBankInstitution) {
		this.nonBankInstitution = nonBankInstitution;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}
	public String getPersonalAccountType() {
		return personalAccountType;
	}
	public void setPersonalAccountType(String personalAccountType) {
		this.personalAccountType = personalAccountType;
	}
	public TEntity gettEntity() {
		return tEntity;
	}
	public void settEntity(TEntity tEntity) {
	
		this.tEntity = tEntity;
		if(tEntity!=null)
			this.tEntity.setAccount(this);
	}
	public List<Signatory> getSignatory() {
		return signatory;
	}
	public void setSignatory(List<Signatory> signatory) {
		
		this.signatory.clear();
		if(signatory!=null)
			for(Signatory sig: signatory) {
				sig.setAccount(this);
				this.signatory.add(sig);
			}
	}
	
	public String getOpened() {
		return opened;
	}
	public void setOpened(String opened) {
		this.opened = opened;
	}
	public String getClosed() {
		return closed;
	}
	public void setClosed(String closed) {
		this.closed = closed;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getDateBalance() {
		return dateBalance;
	}
	public void setDateBalance(String dateBalance) {
		this.dateBalance = dateBalance;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getBeneficiary() {
		return beneficiary;
	}
	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}
	public String getBeneficiaryComment() {
		return beneficiaryComment;
	}
	public void setBeneficiaryComment(String beneficiaryComment) {
		this.beneficiaryComment = beneficiaryComment;
	}
	public String getComments() {
		return comments;
	}
	public String getChangeBeginDate() {
		return changeBeginDate;
	}

	public void setChangeBeginDate(String changeBeginDate) {
		this.changeBeginDate = changeBeginDate;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

//	@JsonIgnore
//	public ReportPartyType getReportPartyType() {
//		return reportPartyType;
//	}
//
//	public void setReportPartyType(ReportPartyType reportPartyType) {
//		this.reportPartyType = reportPartyType;
//	}
	@JsonIgnore
	public TFrom gettFrom() {
		return tFrom;
	}

	public void settFrom(TFrom tFrom) {
		this.tFrom = tFrom;
	}

	@JsonIgnore
	public TTo gettTo() {
		return tTo;
	}

	public void settTo(TTo tTo) {
		this.tTo = tTo;
	}

	public Boolean getIsEntity() {
		return isEntity;
	}

	public void setIsEntity(Boolean isEntity) {
		this.isEntity = isEntity;
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setTAccountIdsZeros() {
		
		this.id = 0;
		if(tEntity!=null)
			tEntity.setTEntityIdsZeros();
		if(signatory!=null) {
			for(Signatory sig: signatory) {
				sig.gettPerson().setTPersonIdsZeros(sig.gettPerson());
			}
		}
	}

	@Override
	public String toString() {
		return "TAccount [id=" + id + ", institutionName=" + institutionName + ", institutionCode=" + institutionCode
				+ ", swift=" + swift + ", nonBankInstitution=" + nonBankInstitution + ", branch=" + branch
				+ ", account=" + account + ", currencyCode=" + currencyCode + ", accountName=" + accountName + ", iban="
				+ iban + ", clientNumber=" + clientNumber + ", personalAccountType=" + personalAccountType
				+ ", tEntity=" + tEntity + ", signatory=" + signatory + ", opened=" + opened + ", closed=" + closed
				+ ", balance=" + balance + ", dateBalance=" + dateBalance + ", statusCode=" + statusCode
				+ ", beneficiary=" + beneficiary + ", beneficiaryComment=" + beneficiaryComment + ", comments="
				+ comments + ", isEntity=" + isEntity + ", isReviewed=" + isReviewed  + ", tFrom=" + tFrom + ", tTo=" + tTo + "]";
	}


}
