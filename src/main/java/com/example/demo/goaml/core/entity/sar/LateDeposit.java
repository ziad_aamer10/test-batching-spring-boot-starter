package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class LateDeposit {
	
	@NotNull
	private boolean value = false;

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}
	
	

}
