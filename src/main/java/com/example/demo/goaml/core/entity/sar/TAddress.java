package com.example.demo.goaml.core.entity.sar;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(schema = "target")
@Audited
public class TAddress {
	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TAddress")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TAddress",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TAddress"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	
	private int Id;
	
	
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "addresses_id")
//    private Addresses addresses;
//	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "entity_id")
    private TEntity entity;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private TPerson person;
	
	private String addressType;
    private String address;
    private String town;
    private String city;
    private String zip;
    private String countryCode;
    private String state;
    private String addressComments;
    //codes: person employer
    //private String personAddressType;
    
//    @OneToOne(mappedBy = "employerAddress", cascade = CascadeType.ALL,
//            fetch = FetchType.LAZY, optional = false)
//    private TPerson personEmpAdd;
    
    
    
    
	public TAddress() {
		
	}

	public TAddress(int id,  String addressType, String address, String town, String city, String zip,
			String countryCode, String state, String addressComments) {
		super();
		Id = id;
		
		this.addressType = addressType;
		this.address = address;
		this.town = town;
		this.city = city;
		this.zip = zip;
		this.countryCode = countryCode;
		this.state = state;
		this.addressComments = addressComments;
	}

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddressComments() {
		return addressComments;
	}
	public void setAddressComments(String addressComments) {
		this.addressComments = addressComments;
	}
	
//	@JsonIgnore
//	public Addresses getAddresses() {
//		return addresses;
//	}
//
//	public void setAddresses(Addresses addresses) {
//		this.addresses = addresses;
//	}

	
	@JsonIgnore
	public TEntity getEntity() {
		return entity;
	}

	
	public void setEntity(TEntity entity) {
		
		this.entity = entity;
	}
	
//	private boolean sameEntityAsFormer(Person newOwner) {
//		  return owner==null? newOwner == null : owner.equals(newOwner);
//	}
	
	@Override
	public String toString() {
		return "Address [Id=" + Id + ", addressType=" + addressType + ", address=" + address + ", town=" + town
				+ ", city=" + city + ", zip=" + zip + ", countryCode=" + countryCode + ", state=" + state
				+ ", addressComments=" + addressComments + "]";
	}
	
	@JsonIgnore
	public TPerson getPerson() {
		return person;
	}

	public void setPerson(TPerson person) {
		this.person = person;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((addressComments == null) ? 0 : addressComments.hashCode());
		result = prime * result + ((addressType == null) ? 0 : addressType.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((town == null) ? 0 : town.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TAddress other = (TAddress) obj;
		if (Id != other.Id)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (addressComments == null) {
			if (other.addressComments != null)
				return false;
		} else if (!addressComments.equals(other.addressComments))
			return false;
		if (addressType == null) {
			if (other.addressType != null)
				return false;
		} else if (!addressType.equals(other.addressType))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (town == null) {
			if (other.town != null)
				return false;
		} else if (!town.equals(other.town))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}

	
    
	
}
