package com.example.demo.goaml.core.entity.sar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


@Embeddable
public class PersonPhones {
	
	@OneToMany(mappedBy = "person",
			cascade = CascadeType.ALL,
	 orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TPhone> phone;
	
	PersonPhones() {
		this.phone = new ArrayList<TPhone>(); 
	}
	
	public List<TPhone> getPhone() {
		return phone;
	}

	public void setPhone(List<TPhone> phone) {
		
		if(phone==null)
		{
			this.phone.clear();
			return;
		}
		
		this.phone = phone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonPhones other = (PersonPhones) obj;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}


	
	

//	public void setPhone(List<TPhone> phone) {
//		this.phone = phone;
//		if(phone!=null)
//		for(TPhone phones: this.phone) {
//			phones.setPerson(this);
//		}
//		
//	}
	
}
