package com.example.demo.goaml.core.entity.sar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Parameter;
//import org.hibernate.annotations.Cascade;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//import com.test.demo.reportingPerson.Address;
//import com.test.demo.reportingPerson.Identification;
//import com.test.demo.reportingPerson.Phone;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(schema="target")
@Audited
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class TPerson {

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_TPerson")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_TPerson",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_TPerson"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;
	private String gender;
	private String title;
	private String firstName;

	@Column(name="DTYPE", insertable=false, updatable=false)
	private String DTYPE;
	
	private String middleName;
	private String prefix;

	private String lastName;

	private String birthdate;

	private String birthPlace;

	private String mothersName;
	private String alias;

	private String ssn;

	private String passportNumber;

	private String passportCountry;

	private String idNumber;

	private String nationality1;
	private String nationality2;
	private String nationality3;

	private String residence;

//	@OneToOne(mappedBy = "person",
//			cascade = CascadeType.ALL,
//			fetch = FetchType.LAZY, optional = false)
	@Embedded
	private PersonPhones phones ;
	
	@Embedded
	private PersonAddresses addresses;
	
//	@OneToMany(mappedBy = "person",
//			cascade = CascadeType.ALL,
//	 orphanRemoval = true, fetch = FetchType.LAZY)
//	private List<TAddress> addresses = new ArrayList<TAddress>();
	
	
	@ElementCollection
    @CollectionTable(name = "EMAILS", schema="target",joinColumns = @JoinColumn(name = "PERSON_ID"))
	@Column(name="email")
	@OnDelete(action= OnDeleteAction.CASCADE)
	@JoinColumn(name = "PERSON_ID")
	private List<String> email;

	private String occupation;

	private String employerName;

	@OneToOne(mappedBy = "person",
			cascade = CascadeType.ALL,orphanRemoval = true,
			fetch = FetchType.LAZY)
	private EmployerAddressId employerAddressId;
	
	@OneToOne(mappedBy = "person",
			cascade = CascadeType.ALL,orphanRemoval = true,
			fetch = FetchType.LAZY)
	private EmployerPhoneId employerPhoneId;

	@OneToMany(mappedBy = "person",
			cascade = CascadeType.ALL, orphanRemoval = true)
	private List<TPersonIdentification> identification;
	

	private Boolean deceased;

	private String dateDeceased;

	private String taxNumber;

	private String taxRegNumber;

	private String sourceOfWealth;
	private String comments;
	
//	@OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "reportPartyType_id")
//    private ReportPartyType reportPartyType;
	
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "signatory_id")
	private Signatory signatory;
	
//	@OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "director_id")
//	private DirectorId directorId;
	
	@OneToOne(mappedBy = "fromPerson",
			fetch = FetchType.LAZY)
	private TFrom tFrom;
	
	@OneToOne(mappedBy = "toPerson",
			fetch = FetchType.LAZY)
	private TTo tTo;
	
	@OneToOne(mappedBy = "tConductor",
			fetch = FetchType.LAZY)
	private TFrom tFromConductor;
	
	private String partyNumber;
	
	@Column(name="is_reviewed")
	private Boolean isReviewed;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Transient
	private String changeBeginDate;
	
	@Transient
	private Character employeeInd;

	public TPerson() {
		super();
		this.email = new ArrayList<>();
		this.identification = new ArrayList<TPersonIdentification>();
		this.addresses = new PersonAddresses();
		this.phones = new PersonPhones();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(String passportCountry) {
		this.passportCountry = passportCountry;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getNationality1() {
		return nationality1;
	}

	public void setNationality1(String nationality1) {
		this.nationality1 = nationality1;
	}

	public String getNationality2() {
		return nationality2;
	}

	public void setNationality2(String nationality2) {
		this.nationality2 = nationality2;
	}

	public String getNationality3() {
		return nationality3;
	}

	public void setNationality3(String nationality3) {
		this.nationality3 = nationality3;
	}

	public String getResidence() {
		return residence;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}
	
	
//	public List<TPhone> getPhones() {
//		return phones;
//	}
//
//	public void setPhones(List<TPhone> phones) {
//		this.phones = phones;
//		
//		for(TPhone phone: this.phones) {
//			phone.setPerson(this);
//		}
//	}

//	public List<TAddress> getAddresses() {
//		return addresses;
//	}
//
//	public void setAddresses(List<TAddress> addresses) {
//		this.addresses = addresses;
//		
//		for(TAddress address: this.addresses) {
//			address.setPerson(this);
//		}
//	}
	
	

	public PersonPhones getPhones() {
		return phones;
	}

	public void setPhones(PersonPhones phones) {
		
		if(phones==null)
		{
			this.phones = new PersonPhones();
			return;
		}
		
		this.phones = phones;
		
		if(phones.getPhone()!=null)
			for(TPhone phone: phones.getPhone())
				phone.setPerson(this);	
		
	}

	public List<String> getEmail() {
		return email;
	}



	public void setEmail(List<String> email) {
		this.email = email;
	}

	
	public PersonAddresses getAddresses() {
		return addresses;
	}

	public void setAddresses(PersonAddresses addresses) {
		
		if(addresses==null)
		{
			this.addresses = new PersonAddresses();
			return;
		}
		
		this.addresses = addresses;
		if(addresses.getAddress()!=null)
			for(TAddress address2: addresses.getAddress()) {
				address2.setPerson(this);
			}
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public List<TPersonIdentification> getIdentification() {
		return identification;
	}

	public void setIdentification(List<TPersonIdentification> identification) {
		
//		this.identification = identification;
//		if(identification!=null)
//		for(TPersonIdentification identi: this.identification) {
//			identi.setPerson(this);
//		}
		
		this.identification.clear();
		if(identification!=null)
		{
			for(TPersonIdentification identi: identification) {
				identi.setPerson(this);
				this.identification.add(identi);
			}
		}
	}

	public Boolean getDeceased() {
		return deceased;
	}

	public void setDeceased(Boolean deceased) {
		this.deceased = deceased;
	}

	public String getDateDeceased() {
		return dateDeceased;
	}

	public void setDateDeceased(String dateDeceased) {
		this.dateDeceased = dateDeceased;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getTaxRegNumber() {
		return taxRegNumber;
	}

	public void setTaxRegNumber(String taxRegNumber) {
		this.taxRegNumber = taxRegNumber;
	}

	public String getSourceOfWealth() {
		return sourceOfWealth;
	}

	public void setSourceOfWealth(String sourceOfWealth) {
		this.sourceOfWealth = sourceOfWealth;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	

	


	public EmployerAddressId getEmployerAddressId() {
		return employerAddressId;
	}



	public void setEmployerAddressId(EmployerAddressId employerAddressId) {
		this.employerAddressId = employerAddressId;
		if(employerAddressId!=null)
		this.employerAddressId.setPerson(this);	
	}



	
	
	public EmployerPhoneId getEmployerPhoneId() {
		return employerPhoneId;
	}



	public void setEmployerPhoneId(EmployerPhoneId employerPhoneId) {
		this.employerPhoneId = employerPhoneId;
		if(employerPhoneId!=null)
		this.employerPhoneId.setPerson(this);	
	}





//	@JsonIgnore
//	public ReportPartyType getReportPartyType() {
//		return reportPartyType;
//	}
//
//
//
//	public void setReportPartyType(ReportPartyType reportPartyType) {
//		this.reportPartyType = reportPartyType;
//	}


	@JsonIgnore
	public Signatory getSignatory() {
		return signatory;
	}



	public void setSignatory(Signatory signatory) {
		this.signatory = signatory;
	}


	@JsonIgnore
	public TFrom gettFrom() {
		return tFrom;
	}



	public void settFrom(TFrom tFrom) {
		this.tFrom = tFrom;
	}


	@JsonIgnore
	public TFrom gettFromConductor() {
		return tFromConductor;
	}



	public void settFromConductor(TFrom tFromConductor) {
		this.tFromConductor = tFromConductor;
	}


	@JsonIgnore
	public TTo gettTo() {
		return tTo;
	}



	public void settTo(TTo tTo) {
		this.tTo = tTo;
	}



	public String getPartyNumber() {
		return partyNumber;
	}



	public void setPartyNumber(String partyNumber) {
		this.partyNumber = partyNumber;
	}

	public String getDTYPE() {
		return DTYPE;
	}

	public void setDTYPE(String dTYPE) {
		DTYPE = dTYPE;
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}

	public String getChangeBeginDate() {
		return changeBeginDate;
	}

	public void setChangeBeginDate(String changeBeginDate) {
		this.changeBeginDate = changeBeginDate;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@JsonIgnore
	public Character getEmployeeInd() {
		return employeeInd;
	}

	public void setEmployeeInd(Character employeeInd) {
		this.employeeInd = employeeInd;
	}

	@Override
	public String toString() {
		return "TPerson [id=" + id + ", gender=" + gender + ", title=" + title + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", prefix=" + prefix + ", lastName=" + lastName + ", birthdate="
				+ birthdate + ", birthPlace=" + birthPlace + ", mothersName=" + mothersName + ", alias=" + alias
				+ ", ssn=" + ssn + ", passportNumber=" + passportNumber + ", passportCountry=" + passportCountry
				+ ", idNumber=" + idNumber + ", nationality1=" + nationality1 + ", nationality2=" + nationality2
				+ ", nationality3=" + nationality3 + ", residence=" + residence + ", phones=" + phones + ", addresses="
				+ addresses + ", email=" + email + ", occupation=" + occupation + ", employerName=" + employerName
				+ ", employerAddressId=" + employerAddressId + ", employerPhoneId=" + employerPhoneId
				+ ", identification=" + identification + ", deceased=" + deceased + ", dateDeceased=" + dateDeceased
				+ ", taxNumber=" + taxNumber + ", taxRegNumber=" + taxRegNumber + ", sourceOfWealth=" + sourceOfWealth
				+ ", comments=" + comments + ", signatory=" + signatory
				+ ", tFrom=" + tFrom + ", tTo=" + tTo + ", tFromConductor=" + tFromConductor + ", partyNumber="
				+ partyNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((birthPlace == null) ? 0 : birthPlace.hashCode());
		result = prime * result + ((birthdate == null) ? 0 : birthdate.hashCode());
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((dateDeceased == null) ? 0 : dateDeceased.hashCode());
		result = prime * result + ((deceased == null) ? 0 : deceased.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((employerAddressId == null) ? 0 : employerAddressId.hashCode());
		result = prime * result + ((employerName == null) ? 0 : employerName.hashCode());
		result = prime * result + ((employerPhoneId == null) ? 0 : employerPhoneId.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + id;
		result = prime * result + ((idNumber == null) ? 0 : idNumber.hashCode());
		result = prime * result + ((identification == null) ? 0 : identification.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((mothersName == null) ? 0 : mothersName.hashCode());
		result = prime * result + ((nationality1 == null) ? 0 : nationality1.hashCode());
		result = prime * result + ((nationality2 == null) ? 0 : nationality2.hashCode());
		result = prime * result + ((nationality3 == null) ? 0 : nationality3.hashCode());
		result = prime * result + ((occupation == null) ? 0 : occupation.hashCode());
		result = prime * result + ((partyNumber == null) ? 0 : partyNumber.hashCode());
		result = prime * result + ((passportCountry == null) ? 0 : passportCountry.hashCode());
		result = prime * result + ((passportNumber == null) ? 0 : passportNumber.hashCode());
		result = prime * result + ((phones == null) ? 0 : phones.hashCode());
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		result = prime * result + ((residence == null) ? 0 : residence.hashCode());
		result = prime * result + ((sourceOfWealth == null) ? 0 : sourceOfWealth.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		result = prime * result + ((taxNumber == null) ? 0 : taxNumber.hashCode());
		result = prime * result + ((taxRegNumber == null) ? 0 : taxRegNumber.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TPerson other = (TPerson) obj;
		if (addresses == null) {
			if (other.addresses != null)
				return false;
		} else if (!addresses.equals(other.addresses))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (birthPlace == null) {
			if (other.birthPlace != null)
				return false;
		} else if (!birthPlace.equals(other.birthPlace))
			return false;
		if (birthdate == null) {
			if (other.birthdate != null)
				return false;
		} else if (!birthdate.equals(other.birthdate))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (dateDeceased == null) {
			if (other.dateDeceased != null)
				return false;
		} else if (!dateDeceased.equals(other.dateDeceased))
			return false;
		if (deceased == null) {
			if (other.deceased != null)
				return false;
		} else if (!deceased.equals(other.deceased))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (employerAddressId == null) {
			if (other.employerAddressId != null)
				return false;
		} else if (!employerAddressId.equals(other.employerAddressId))
			return false;
		if (employerName == null) {
			if (other.employerName != null)
				return false;
		} else if (!employerName.equals(other.employerName))
			return false;
		if (employerPhoneId == null) {
			if (other.employerPhoneId != null)
				return false;
		} else if (!employerPhoneId.equals(other.employerPhoneId))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id != other.id)
			return false;
		if (idNumber == null) {
			if (other.idNumber != null)
				return false;
		} else if (!idNumber.equals(other.idNumber))
			return false;
		if (identification == null) {
			if (other.identification != null)
				return false;
		} else if (!identification.equals(other.identification))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (mothersName == null) {
			if (other.mothersName != null)
				return false;
		} else if (!mothersName.equals(other.mothersName))
			return false;
		if (nationality1 == null) {
			if (other.nationality1 != null)
				return false;
		} else if (!nationality1.equals(other.nationality1))
			return false;
		if (nationality2 == null) {
			if (other.nationality2 != null)
				return false;
		} else if (!nationality2.equals(other.nationality2))
			return false;
		if (nationality3 == null) {
			if (other.nationality3 != null)
				return false;
		} else if (!nationality3.equals(other.nationality3))
			return false;
		if (occupation == null) {
			if (other.occupation != null)
				return false;
		} else if (!occupation.equals(other.occupation))
			return false;
		if (partyNumber == null) {
			if (other.partyNumber != null)
				return false;
		} else if (!partyNumber.equals(other.partyNumber))
			return false;
		if (passportCountry == null) {
			if (other.passportCountry != null)
				return false;
		} else if (!passportCountry.equals(other.passportCountry))
			return false;
		if (passportNumber == null) {
			if (other.passportNumber != null)
				return false;
		} else if (!passportNumber.equals(other.passportNumber))
			return false;
		if (phones == null) {
			if (other.phones != null)
				return false;
		} else if (!phones.equals(other.phones))
			return false;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		if (residence == null) {
			if (other.residence != null)
				return false;
		} else if (!residence.equals(other.residence))
			return false;
		if (sourceOfWealth == null) {
			if (other.sourceOfWealth != null)
				return false;
		} else if (!sourceOfWealth.equals(other.sourceOfWealth))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		if (taxNumber == null) {
			if (other.taxNumber != null)
				return false;
		} else if (!taxNumber.equals(other.taxNumber))
			return false;
		if (taxRegNumber == null) {
			if (other.taxRegNumber != null)
				return false;
		} else if (!taxRegNumber.equals(other.taxRegNumber))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	public void setTPersonIdsZeros(TPerson tPerson) {

		tPerson.setId(0);
		if(tPerson.getEmployerAddressId()!=null)
			tPerson.getEmployerAddressId().setId(0);
		if(tPerson.getEmployerPhoneId()!=null)
			tPerson.getEmployerPhoneId().setId(0);

		if(tPerson.getAddresses()!=null && tPerson.getAddresses().getAddress() !=null)
			for(TAddress address2: tPerson.getAddresses().getAddress()) {
				address2.setId(0);
			}

		if(tPerson.getPhones()!=null && tPerson.getPhones().getPhone()!=null)
			for(TPhone phone: tPerson.getPhones().getPhone()) {
				phone.setId(0);	

			}

		if(tPerson.getIdentification()!=null)
		{
			for(TPersonIdentification identi: tPerson.getIdentification()) {
				identi.setId(0);
			}
		}

	}


}
