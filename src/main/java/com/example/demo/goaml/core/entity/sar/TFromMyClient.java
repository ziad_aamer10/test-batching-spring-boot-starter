package com.example.demo.goaml.core.entity.sar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
public class TFromMyClient extends TFrom{

	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionMyClient_id")
	private Transaction transactionMyClient;

	@JsonIgnore
	public Transaction getTransaction() {
		return transactionMyClient;
	}

	public void setTransaction(Transaction transactionMyClient) {
		this.transactionMyClient = transactionMyClient;
	}
	
	
	
}
