package com.example.demo.goaml.core.entity.sar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="target")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class Signatory {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator_Signatory")
	  @GenericGenerator(
	          name = "hilo_sequence_generator_Signatory",
	          strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	          parameters = {
	                  @Parameter(name = "sequence_name", value = "hilo_sequence_generator_Signatory"),
	                  @Parameter(name = "initial_value", value = "1"),
	                  @Parameter(name = "increment_size", value = "10"),
	                  @Parameter(name = "optimizer", value = "hilo")
	          })
	private int id;
	
	
	private Boolean isPrimary;
	
	@OneToOne(mappedBy = "signatory",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = true)
	private TPerson tPerson;
	
	private String role;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private TAccount account;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public TPerson gettPerson() {
		return tPerson;
	}

	public void settPerson(TPerson tPerson) {
		this.tPerson = tPerson;
		if(tPerson!=null)
		this.tPerson.setSignatory(this);
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	@JsonIgnore
	public TAccount getAccount() {
		return account;
	}

	public void setAccount(TAccount account) {
		this.account = account;
	}

	
	
}
