package com.example.demo.goaml.core.entity.sar;

 

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Embeddable
public class PersonAddresses {
	
	
	
	
	@OneToMany(mappedBy = "person",
			cascade = CascadeType.ALL,
	 orphanRemoval = true, fetch = FetchType.LAZY)
	private List<TAddress> address;
	
	public PersonAddresses(){
		this.address = new ArrayList<TAddress>();
	}



	public List<TAddress> getAddress() {
		return address;
	}

	public void setAddress(List<TAddress> address) {
		if(address==null) {
			this.address.clear();
			return;
		}
		this.address = address;
		
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonAddresses other = (PersonAddresses) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		return true;
	}	

}
